@extends('admin.master')
@section('content')
<div class="container mt-5 ">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <h1 class="h3 jumbotron py-4 text-center text-dark">Update Configration</h1>
                <div class="card-body">
                @if(Session::has('error'))
                    <div class="alert alert-danger">{{Session::get('error')}}</div>
                @endif
                <form method="post" action="{{url('config/'.$config->id)}}">
                    @csrf()
                    @method('PUT')
                    {{-- <div class="form-group">
                        <label for="orgination_name">orgination Name<span class="error">*</span></label>
                        <input type="text" class="form-control" id="orgination_name" name="orgination_name" value="{{$config->orgination_name}}">
                        @if($errors->has('orgination_name'))
                            <label  class="alert alert-danger">{{$errors->first('orgination_name')}}</label>
                        @endif 
                    </div> --}}
                    
                    <div class="form-group">
                        <label for="admin_email">Admin Email<span class="error">*</span></label>
                        <input type="text" class="form-control" id="admin_email" name="admin_email" value="{{$config->admin_email}}">
                        @if($errors->has('admin_email'))
                            <label  class="alert alert-danger">{{$errors->first('admin_email')}}</label>
                        @endif 
                    </div>
                    <div class="form-group">
                        <label for="notification_email">Notification Email<span class="error">*</span></label>
                        <input type="text" class="form-control" id="notification_email" name="notification_email" value="{{$config->notification_email}}">
                        @if($errors->has('notification_email'))
                            <label  class="alert alert-danger">{{$errors->first('notification_email')}}</label>
                        @endif 
                    </div>
                    {{-- <div class="form-group">
                        <label for="logo">Logo<span class="error">*</span></label>
                        <input type="file" class="form-control" id="logo" name="logo" value="{{$config->logo}}">
                        <span class="form-text small text-muted px-2">Note: your 300px width image</span>
                        @if($errors->has('logo'))
                            <label  class="alert alert-danger">{{$errors->first('logo')}}</label>
                        @endif 
                    </div> --}}
                   
                    <div class="mt-2">
                        <button type="submit" class="btn btn-success">Update</button>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection