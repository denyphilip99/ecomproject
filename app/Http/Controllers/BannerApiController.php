<?php

namespace App\Http\Controllers;
use App\Models\Banner;
use Illuminate\Http\Request;

class BannerApiController extends Controller
{
    //
    public function index()
    {
        //
        return Banner::all();
    }
    public function create()
    {
        //
    }
    public function store(Request $req)
    {
        $validate=$req->validate([
                'caption'=>'required|min:5|max:150',
                'image'=>'required|mimes:jpg,png,jpeg',
                'status'=>'required',
            ]);
        if($validate){
           $caption=$req->caption;
           $file=$req->file('image');
           $dest=public_path('/uploads');
           $fname="Image-".rand()."-".time().".".$file->extension();
           if($file->move($dest,$fname))
           {
               Banner::create([
                'caption'=>$caption,
                'image'=>$fname,
                'status'=>$req->status,

               ]);
                return ['error'=>"inserted successfully"];
            }
            else{
                   $path=public_path()."uploads/".$fname;
                   unlink($path);
                   return ['error'=>"Insertion failed"];
               }
           }
           else {
               return ['error'=>"All fields are required"];
           }
    }
    public function show($id)
    {
      return Banner::findOrfail($id);
    }

}
