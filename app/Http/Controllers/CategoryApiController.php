<?php

namespace App\Http\Controllers;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return Category::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        //
        $validate=$req->validate([
            'name'=>'required|min:2|unique:categories|alpha',
            'description'=>'required|min:10|max:255',
            'status'=>'required',
        ]);
        if($validate){
            Category::create([
                'name'=>$req->name,
                'description'=>$req->description,
                'status'=>$req->status,
            ]);
            return ['error'=>"inserted successfully"];
        }
        else{
            return ['error'=>"Insertion failed.All fields are required"];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return Category::findOrfail($id);
    }
    public function update(Request $request, $id)
    {
        //
    }
    public function destroy($id)
    {
        //
        if($id){
        $data=Category::findOrfail($id);
        $data->delete();
        return ["error"=>"data deleted successfully"];
        }
        else{
            return ["error"=>"data could not be deleted"];
        }
    }
}
