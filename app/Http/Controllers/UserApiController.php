<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class UserApiController extends Controller
{
    //
    public function index()
    {
        //
        return User::all();
    }
    public function store(Request $req)
    {
        $validate=$req->validate([
            'firstname'=>'required|min:2|alpha',
            'lastname'=>'required|min:2|alpha',
            'email'=>'required|unique:users|email',
            'password'=>'required|min:6|max:12',
            'cpassword'=>'required|min:6|max:12|required_with:password|same:password',
            'status'=>'required',
         
        ]);
        if($validate){
            User::create([
                'firstname'=>$req->firstname,
                'lastname'=>$req->lastname,
                'email'=>$req->email,
                'password'=>Hash::make($req->password),
                'status'=>$req->status,
                'role_id' => $req->role ?? '5',
            
            ]);
            return ['error'=>"inserted successfully"];
        }
        else{
            return ['error'=>"insertion failed.All fields required"];
        }
    }
    public function show($id)
    {
      return User::findOrfail($id);
    }
}
