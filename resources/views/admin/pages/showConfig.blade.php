@extends('admin.master')
@section('content')
<div class="container-fluid mt-5">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="jumbotron py-4">
                    <h1 class="float-left h3">Configration</h1>
                    <a href="{{url('config/'.$config->id.'/edit')}}" class="btn btn-primary float-right">Update Setting</a>
                </div>
                <div class="card-body pt-0">
                    <table class="table" id="mytable">
                        {{-- <tr>
                            <th>Website Name</th>
                            <td>{{$config->orgination_name}}</td>
                        </tr> --}}
                        <tr>
                            <th>Admin Email</th>
                            <td>{{$config->admin_email}}</td>
                        </tr>
                        {{-- <tr>
                            <th>Logo Image</th>
                            <td>{{$config->logo_image}}</td>
                        </tr> --}}
                        {{-- <tr>
                            <th>GST</th>
                            <td>{{$config->gst_no}}</td>
                        </tr> --}}
                        <tr>
                            <th>Notification Email</th>
                            <td>{{$config->Notification_email}}</td>
                        </tr>
                        {{-- <tr>
                            <th>Change Admin Password</th>
                            <td><a href="">Change</a></td>
                        </tr> --}}
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection