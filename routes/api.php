<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\JwtController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
// Route::group(['middleware'=>'api'], function ($router) {
//     Route::post('logout',[JwtController::class,'logout']);
//     Route::post('refresh',[JwtController::class,'refresh']);
//     Route::post('profile',[JwtController::class,'profile']);
//     Route::post('login',[JwtController::class,'login']);
//     Route::post('register',[JwtController::class,'register']);
   
//     });

    Route::group(['middleware'=>['jwt']], function ($router) {
        Route::get('getuser',[JwtController::class,'get_user']);
        Route::post('logout',[JwtController::class,'logout']);
        Route::post('refresh',[JwtController::class,'refresh']);
        Route::post('profile',[JwtController::class,'profile']);
    });
    Route::post('login',[JwtController::class,'login']);
    Route::post('register',[JwtController::class,'register']);
// Route::apiResource("categories",CategoryApiController::class);
// Route::apiResource("users",UserApiController::class);
// Route::apiResource("coupons",CouponApiController::class);
// Route::apiResource("banners",BannerApiController::class);
// Route::apiResource("products",ProductApiController::class);

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();

