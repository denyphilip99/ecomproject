<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use App\Models\ProductAttributeAssoc;
use App\Models\ProductCategory;
use App\Models\ProductImage;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products=Product::paginate(8);
        // $attributes = ProductAttributeAssoc::with('Product')->get();
        // $categories = ProductCategory::with('Category','Product')->get();
        // $images = ProductImage::with('Product')->get();

        return view('admin.pages.showproduct',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $catdata=Category::all();
        return view('admin.pages.addproduct',compact('catdata'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        $validate=$req->validate([
            'pname'=>'required|min:2',
            'price'=>'required|numeric',
            'quantity'=>'required|numeric',
            'features'=>'required|min:6|max:255',
            'file'=>'required',
         
        ]);
        if($validate){
            $image=array();
            $imageextension=['jpeg','png','gif','jpg'];
            $files=$req->file('file');
            if( $files && $imageextension){
                foreach($files as $file)
                {
                    $dest='public/uploads/';
                    $filename="Image-".rand()."-".time().".".$file->extension();
                    $image_url=$dest.$filename;
                    $file->move($dest,$filename);
                    $image[]=$image_url;
                }
                $product=Product::create([
                    'pname'=>$req->pname,
                ]);
                if($product){
                    $pid=$product->id;
                    ProductCategory::create([
                        'products_id'=>$pid,
                        'categories_id'=>$req->category,
                    ]);
                    ProductAttributeAssoc::create([
                        'products_id'=>$pid,
                        'price'=>$req->price,
                        'quantity'=>$req->quantity,
                        'features'=>$req->features,
                    ]);
                    ProductImage::create([
                        'products_id'=>$pid,
                        'images'=>implode('|',$image),
                    ]);
                }
                return redirect('products'); 
            }
            else {
                $path=public_path()."uploads/".$filename;
                unlink($path);
                return back()->with('error','uploading error');
            }    
        }
        else{
            return back()->with('error', "something went wrong") ;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=Category::all();
        $product=Product::where('id',$id)->first();
        return view('admin.pages.updateproduct',compact('data','product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
        $validate=$req->validate([
            'pname'=>'required|min:2',
            'price'=>'required|numeric',
            'quantity'=>'required|numeric',
            'features'=>'required|min:6|max:255',
            'file'=>'required',
         
        ]);
        if($validate){
            $image=array();
            $imageextension=['jpeg','png','gif','jpg'];
            $files=$req->file('file');
            if( $files && $imageextension){
                foreach($files as $file)
                {
                    $dest='public/uploads/';
                    $filename="Image-".rand()."-".time().".".$file->extension();
                    $image_url=$dest.$filename;
                    $file->move($dest,$filename);
                    $image[]=$image_url;
                }
                Product::where('id',$id)->update([
                    'pname'=>$req->pname,
                ]);
                ProductCategory::where('id',$id)->update([
                    'categories_id'=>$req->category,
                ]);
                ProductAttributeAssoc::where('id',$id)->update([
                    'price'=>$req->price,
                    'quantity'=>$req->quantity,
                    'features'=>$req->features,
                ]);
                ProductImage::where('id',$id)->update([
                    'images'=>implode('|',$image),
                ]);
                return redirect('products'); 
            }
            else {
                $path=public_path()."uploads/".$filename;
                unlink($path);
                return back()->with('error','uploading error');
            }    
        }
        else{
            return back()->with('error', "something went wrong") ;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product_data=Product::find($id);
        if($product_data->delete()){
            return response()->json(['msg'=>"product deleted"]);
        }
        else{
            return response()->json(['msg'=>"product could not be deleted"]);
        }
    }
}
