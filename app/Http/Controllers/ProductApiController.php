<?php

namespace App\Http\Controllers;
use App\Models\Product;

use App\Models\Category;
use App\Models\ProductAttributeAssoc;
use App\Models\ProductCategory;
use App\Models\ProductImage;
use Illuminate\Http\Request;

class ProductApiController extends Controller
{
    //
    public function index()
    {
        //
        return Product::all();
    }
    public function store(Request $req)
    {
        $validate=$req->validate([
            'pname'=>'required|min:2',
            'price'=>'required|numeric',
            'quantity'=>'required|numeric',
            'features'=>'required|min:6|max:255',
            'file'=>'required',
         
        ]);
        if($validate){
            $image=array();
            $imageextension=['jpeg','png','gif','jpg'];
            $files=$req->file('file');
            if( $files && $imageextension){
                foreach($files as $file)
                {
                    $dest='public/uploads/';
                    $filename="Image-".rand()."-".time().".".$file->extension();
                    $image_url=$dest.$filename;
                    $file->move($dest,$filename);
                    $image[]=$image_url;
                }
                $product=Product::create([
                    'pname'=>$req->pname,
                ]);
                if($product){
                    $pid=$product->id;
                    ProductCategory::create([
                        'products_id'=>$pid,
                        'categories_id'=>$req->category,
                    ]);
                    ProductAttributeAssoc::create([
                        'products_id'=>$pid,
                        'price'=>$req->price,
                        'quantity'=>$req->quantity,
                        'features'=>$req->features,
                    ]);
                    ProductImage::create([
                        'products_id'=>$pid,
                        'images'=>implode('|',$image),
                    ]);
                }
                return ['error'=>"inserted successfully"];
            }
            else {
                $path=public_path()."uploads/".$filename;
                unlink($path);
                return ['error'=>"uploading error"];
            }    
        }
        else{
            return ['error'=>"something went wrong"];
        }
    }
    public function show($id)
    {
      return Product::findOrfail($id);
    }
}
